package com.example.application1.constants

enum class CellConstants(val value: Byte){
    EMPTY(0), EPHEMERAL(1)
}