package com.example.application1

import android.graphics.Point
import com.example.application1.constants.FieldConstants
import com.example.application1.constants.CellConstants
import com.example.application1.helper.array2dOfByte
import com.example.application1.models.Block
import com.example.application1.storage.AppPreferences

class AppModel {
    var score: Int = 0
    private var preferences: AppPreferences? = null // содержит объект AppPreferences для обеспечения доступа к файлу SharedPreferences

    var currentBlock: Block? = null
    var currentState: Statuses = Statuses.AWAITING_START

    private var field: Array<ByteArray> = array2dOfByte( // двумерный массив, служащий в качестве игрового поля
        FieldConstants.ROW_COUNT.value,
        FieldConstants.COLUMN_COUNT.value
    )

    enum class Statuses{
        AWAITING_START, // состояние игры до ее запуска
        ACTIVE, INACTIVE, // состояние игрового процесса
        OVER // статус, принмаемый игрой на момент завершения
    }

    enum class Motions{
        LEFT, RIGHT, DOWN, ROTATE
    }

    fun setPreferences(preferences: AppPreferences?){
        this.preferences = preferences
    }

    fun getCellStatus(row: Int, column: Int): Byte?{ // возвращает состояние ячейки, имеющихся в указанной позиции
        return field[row][column]
    }

    private fun setCellStatus(row: Int, column: Int, status: Byte?){ // устанавливает состояние имеющийся в поле ячейки равным байту
        if(status != null){
            field[row][column] = status
        }
    }

    fun isGameOver(): Boolean{
        return currentState == Statuses.OVER
    }

    fun isGameActive(): Boolean{
        return currentState == Statuses.ACTIVE
    }

    fun isGameAwaitingStart(): Boolean{
        return currentState == Statuses.AWAITING_START
    }

    private fun boostScore(){ // возрастание счета
        score += 10
        if(score > preferences!!.getHighScore())
            preferences?.saveHighScore(score)
    }

    private fun generateNextBlock(){
        currentBlock = Block.createBlock()
    }

    private fun validPosition(position: Point, shape: Array<ByteArray>): Boolean{ //служит для проверки допустимости поступательного движения
        return if(position.y < 0 || position.x < 0){
            false
        } else if(position.y + shape.size > FieldConstants.ROW_COUNT.value){
            false
        }else if(position.x + shape[0].size > FieldConstants.COLUMN_COUNT.value){
            false
        } else{
            for(i in shape.indices){
                for(j in shape[i].indices){
                    val y = position.y + i
                    val x = position.x + j
                    if(CellConstants.EMPTY.value != shape[i][j] &&
                        CellConstants.EMPTY.value != field[y][x]){
                        return false
                    }
                }
            }
            true
        }
    }

    private fun moveValid(position: Point, frameNumber: Int): Boolean{
        val shape: Array<ByteArray> = currentBlock?.getShape(frameNumber)!!

        return validPosition(position, shape)
    }

    fun generateField(action: Motions){ // генерирует обновление поля
        if(isGameActive()){
            resetField()
            val currentBlockNotNull: Block = currentBlock!!
            var frameNumber: Int? = currentBlockNotNull.frameNumber
            val coordinate: Point? = Point()
            coordinate?.x = currentBlockNotNull.position?.x
            coordinate?.y = currentBlockNotNull.position?.y

            when(action){
                Motions.LEFT ->{
                    coordinate?.x = currentBlockNotNull.position?.x?.minus(1)
                }
                Motions.RIGHT ->{
                    coordinate?.x = currentBlockNotNull.position?.x?.plus(1)
                }
                Motions.DOWN ->{
                    coordinate?.y = currentBlockNotNull.position?.y?.plus(1)
                }
                Motions.ROTATE ->{
                    frameNumber = frameNumber?.plus(1)
                    if(frameNumber != null){
                        if(frameNumber >= currentBlockNotNull.frameCount){
                            frameNumber = 0
                        }
                    }
                }
            }
            if(!moveValid(coordinate as Point, frameNumber!!)){
                translateBlock(currentBlockNotNull.position as Point, currentBlockNotNull.frameNumber)
                if(Motions.DOWN == action){
                    boostScore()
                    persistCellData()
                    assessField()
                    generateNextBlock()
                    if(!blockAdditionPossible()){
                        currentState = Statuses.OVER
                        currentBlock = null
                        resetField(false)
                    }
                }
            } else{
                    translateBlock(coordinate, frameNumber)
                    currentBlockNotNull.setState(frameNumber, coordinate)
            }
        }
    }

    private fun resetField(ephemeralCellsOnly: Boolean = true){
        for(i in 0 until FieldConstants.ROW_COUNT.value){
            (0 until FieldConstants.COLUMN_COUNT.value).filter{
                !ephemeralCellsOnly || field[i][it] == CellConstants.EPHEMERAL.value }.forEach{
                    field[i][it] = CellConstants.EMPTY.value
                }
        }
    }

    private  fun persistCellData(){ // сохранение ячеек поля
        for(i in field.indices){
            for(j in 0 until field[i].size){
                var status = getCellStatus(i, j)

                if(status == CellConstants.EPHEMERAL.value){
                    status = currentBlock?.staticValue
                    setCellStatus(i, j, status)
                }
            }
        }
    }

    private fun assessField(){ // сканирование строк поля и проверки заполняемости находящихся в строках ячеек
        for(i in field.indices){
            var emptyCells = 0
            for(j in 0 until field[i].size){
                val status = getCellStatus(i, j)
                val isEmpty = CellConstants.EMPTY.value == status
                if(isEmpty)
                    emptyCells++
            }
            if (emptyCells == 0)
                shiftRows(i)
        }
    }

    private fun translateBlock(position: Point, frameNumber: Int){
        synchronized(field){
            val shape: Array<ByteArray>? = currentBlock?.getShape(frameNumber)
            if(shape != null){
                for(i in shape.indices){
                    for(j in 0 until shape[i].size){
                        val y = position.y + i
                        val x = position.x + j
                        if(CellConstants.EMPTY.value != shape[i][j]){
                            field[y][x] = shape[i][j]
                        }
                    }
                }
            }
        }
    }

    private fun blockAdditionPossible(): Boolean{ // проверка на то, что поле не заполнено и блок может перемещаться
        if(!moveValid(currentBlock?.position as Point, currentBlock?.frameNumber!!)){
            return false
        }
        return true
    }

    private fun shiftRows(nToRow: Int){ // очищение строки и сдвиг на велечину
        if(nToRow > 0){
            for(j in nToRow - 1 downTo 0){
                for(m in 0 until field[j].size){
                    setCellStatus(j + 1, m, getCellStatus(j, m))
                }
            }
        }
        for(j in 0 until field[0].size){
            setCellStatus(0, j, CellConstants.EMPTY.value)
        }
    }

    fun startGame(){
        if(!isGameActive()){
            currentState = Statuses.ACTIVE
            generateNextBlock()
        }
    }

    fun restartGame(){
        resetModel()
        startGame()
    }

    fun endGame(){
        score = 0
        currentState = Statuses.OVER
    }

    private fun resetModel(){
        resetField(false)
        currentState = Statuses.AWAITING_START
        score = 0
    }
}